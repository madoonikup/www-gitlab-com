---
layout: job_family_page
title: "UX Management"
---

# UX Management Roles at GitLab

Managers in the UX department at GitLab see the team as their product. While they are credible as designers and know the details of what Product Designers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

## Manager, Product Design

The Manager, Product Design reports to the Senior Manager, Product Design or the Director of Product Design, and Product Designers report to the Manager, Product Design.

### Job Grade

The Manager, Product Design is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **Product knowledge:** Understand the technology and features of the product areas to which you are assigned, and have working knowledge of the end-to-end GitLab product.
* **Cross-product collaboration:** Proactively identify large, strategic UX opportunities that span the product areas for which you are responsible and the product as a whole. Work with other Managers, Product Design to drive cross-product initiatives.
* **Deliverables:** Review UX deliverables (research, designs, etc.) that your team creates, and provide feedback to ensure high-quality output.
* **Research:** Identify strategic user research initiatives that span multiple stage groups (and possibly the entire product), and work with other Product Design/Research Managers to organize research efforts.
* **UX evangelism:** Communicate the value of UX to cross-functional GitLab team members, and influence the Product Managers you support to prioritize UX initiatives.
* **UX process**: Set up and manage collaborative processes within your team to ensure Product Designers and Researchers are actively working together. Make sure everyone has exposure to the work that is happening within the broader team.
* **Team building:** Hire a world-class team of Product Designers.
* **Public presence:** Engage in social media efforts, including writing blog articles and responding on Twitter, as appropriate.
* **Vision and direction:** Have an awareness of Opportunity Canvas reviews, strategy, and vision of the product areas you're assigned.
* **People management:** Hold regular 1:1s with every member of your team and create Individual Growth Plans with monthly check-ins.

### Specialties

Read more about what a [specialty](/handbook/hiring/vacancies/#definitions) is at GitLab here.

#### FE/UX Foundations

The Foundations team works on building a cohesive and consistent user experience, both visually and functionally. You'll be responsible for leading the direction of the experience design, visual style, and technical tooling of the GitLab product. You'll act as a centralized resource, helping to triage large-scale experience problems as the need arises.

You'll spend your time collaborating with a [cross-functional team](https://about.gitlab.com/handbook/product/categories/#ecosystem-group), helping to implement our [Design System](https://design.gitlab.com/), building comprehensive accessibility standards into our workflows, and defining guidelines and best practices that will inform how teams design and build products. A breakdown of the vision you’ll help to deliver within the FE/UX Foundation category can be found on our [product direction page](https://about.gitlab.com/direction/create/ecosystem/frontend-ux-foundations/).

**What you can expect in a Manager, Product Design FE/UX Foundations role at GitLab:**

* Advocate for good product design practices and bring a deep level of subject matter expertise to the team.
* Act as the experience owner of our [Pajamas Design System](https://design.gitlab.com/).
* Play a key role in both defining the direction of the category and regularly refining/scheduling issues during a given milestone.
* Work with your direct reports to build out a UX strategy for your team.
* Proactively learn other product areas in order to help your Product Designers propose design solutions that work for multiple use cases and scenarios across the product.
* Proactively identify large, strategic UX opportunities within the Foundations team and the product as a whole. Work with other Product Design Managers to drive cross-product initiatives.
* Review UX deliverables (research, designs, and so on) that your team creates, and provide feedback to ensure high-quality output.
* Identify strategic user research initiatives that span multiple product areas, and work with other Product Design/Research Managers to organize research efforts.
* Evangelize the value of UX to cross-functional GitLab team members, and influence the Product Managers you support to prioritize UX initiatives.
* Track coverage of Pajamas components in the GitLab product.
* Set up and manage collaborative processes within your team to ensure Product Designers and UX Researchers are actively working together. Make sure everyone has exposure to the work that is happening within the broader team.
* Foster a safe space for your team, where they’re comfortable sharing feedback and advocating for change they see as necessary. 
* Champion the importance of participation in critiques, content creation, and speaking engagements, while also participating alongside them.
* Hire and retain a world-class team of Product Designers.
* Hold regular 1:1s with every member of your team, and create Individual Growth Plans with monthly check-ins.
* Collaborate with our [VP of UX to define OKRs](https://about.gitlab.com/company/okrs/fy21-q2/) for our design practice. These OKRs will shape your team’s process, define your responsibilities as a manager, and feed into the responsibilities of your direct reports.
* Play a part in the evolution of our design culture as we take the next steps in expansion.

### Requirements

* A minimum of 3 years experience managing a group of designers.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Figma, Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* **Interview with Product Designer** In this interview, the interviewer will want to understand the experience you have as a manager, what type of teams you have led, and your management style. The interviewer will also look to understand how you define strategy, how you work with researchers, how you handle conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, your experience with Design Systems, and your technical ability.
* **Interview with Product Design Manager** In this interview, we want you to share specific examples from your work that provide insight into a problem you solved as part of a project you led. We'll look to understand the size and structure of your team, the goals of the project, how you/the team approached research, how you synthesized research data to inform design decisions, what design standards and guidelines you worked within, how you collaborated with the wider team, and the overall outcome. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result. A formal case study is not required but welcomed.
* **Interview with Director of Product Design** In this interview, the interviewer will want to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you handle conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and your technical ability.
* **Interview with Director of Product (Product Management)** In this interview, the interviewer will want to understand how your career experiences will set you up for success at GitLab. They will also look to understand how you work with cross-functional partners, the domains you've worked in previously, and the types of teams you've led. 
* **Interview with VP of UX** In this interview, the interviewer will want to understand the experience you have as a manager, your experience working remotely, and how these two elements of your career intersect. They will also look to understand your technical skills and the types of products you've worked on previously.  

## Senior Manager, Product Design

The Senior Manager, Product Design reports to the Director of Product Design, and the Product Design Manager reports to the Senior Product Design Manager.

### Job Grade

The Senior Manager, Product Design is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **Performance tracking:** Define and manage performance indicators for the Product Design team by actively contributing to the product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* **Cross-product collaboration:** Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* **Product knowledge:** Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* **Goal setting:** Facilitate the creation and execution of product design [OKRs](https://about.gitlab.com/company/okrs/) in collabration with the Product Design team and UX Leadership.
* **UX evangelism:** Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* **People management:** Coach Managers, Product Design on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* **Skip levels:** Conduct quarterly skip levels with your reports' direct reports.
* **Vision and direction**: Have an awareness of Opportunity Canvas reviews, strategy, and vision of the stages you're assigned.
* **Team building:** Hire and retain a world-class team of Product Designers and Managers, Product Design.

### Requirements

* A minimum of 3 years experience managing Managers, Product Design.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Experience defining the high-level strategy (the why) and helping your team tie design and research back to that strategy.
* Experience driving organizational change with cross-functional stakeholders.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group).
* Ability to use GitLab.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Manager, Product Design. In this interview, the interviewer will focus on understanding your experience with driving design strategy, managing managers, and influencing the wider organization in which you worked. Examples of large, complex projects that had a significant impact on product experience will be helpful. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with Director of Product Design. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led and your management style. The interviewer will also want to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Director of Product (Product Management).
* Interview with VP of Engineering.

## Director of Product Design

The Director of Product Design reports to the VP of UX, and Manager, Product Design and Senior Manager, Product Design report to the Director of Product Design.

The Director of Product Design role extends the Senior Manager, Product Design role.

### Job Grade

The Director of Product Design is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **Performance tracking:** Define and manage performance indicators for the Product Design team by independently managing product design KPIs on the [UX KPIs](/handbook/engineering/ux/performance-indicators/) page in the handbook.
* **Cross-product collaboration:** Actively advocate for Product Design throughout the organization by ensuring Product Design responsibilities are reflected in the Product Development Flow.
* **Product knowledge:** Help drive cross-product workflows by having an awareness of what's happening across all sections through active participation in design reviews, UX Showcases, and Group Conversations.
* **Goal setting:** Independently manage the creation and execution of product design [OKRs](https://about.gitlab.com/company/okrs/) with feedback from the Product Design team and UX Leadership.
* **UX evangelism:** Ensure UX is prioritized by working with product leadership to identify opportunities for validation and better cross-functional collaboration.
* **Design strategy:** Communicate significant product design strategy decisions to leadership and the wider company.
* **People management:** Coach Managers, Product Design on how to conduct 1:1s, growth, and feedback conversations with their direct reports.
* **Skip levels:** Conduct quarterly skip levels with your reports' direct reports.
* **Vision and direction**: Have an awareness of Opportunity Canvas reviews, strategy, and visions across the product. 
* **Team building:** Hire and retain a world-class team of Product Designers and Managers, Product Design.

### Requirements

* A minimum of 10 years experience managing designers, and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Proficiency with pre-visualization software (e.g. Figma, Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with Manager, Product Design or Director of Product Design. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with researchers, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with Director of Product (Product Management).
* Interview with VP of UX.

## Vice President of UX

The Vice President of User Experience will report to the Executive Vice President of Engineering, and Senior Managers and Directors of Product Design, UX Research, and Technical Writing report to them.

### Job Grade

The VP of UX is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* **UX strategy:** Set a UX vision that aligns Product Design, UX Research, and Technical Writing with overall company objectives.
* Communicate broadly about important UX initiatives, setting an ambitious UX vision for the department, product, and company.
* **UX resources:** Manage the UX budget, including compensation planning, non-headcount budget allocation, and tradeoff decisions.
* **Executive collaboration:** Interface regularly with executives on important decisions.
* **UX initiatives:** Identify ways to elevate the GitLab product experience, manage initiatives to address those concerns, and track and communicate about progress.
* **Coach UX leaders:** Help UX leaders grow their skills and leadership experience.
* **Culture:** Foster an open and collaborative culture based on trust in the UX department, where everyone feels empowered to do their best work.
* **Cross-product collaboration:** Ensure that UX is well-integrated into the [Product Development Flow](/handbook/product-development-flow/), and advocate for process changes that help product management, engineering, and UX work together to build a great experience.
* **Design system:** Define and promote design guidelines, best practices, and standards, and help to drive GitLab's [design system](https://design.gitlab.com/) forward at a strategic level.
* **Research evangelism:** Work with product leadership to prioritize research efforts, so that we validate whether we're solving the right problems in the right ways.
* **Democratize UX:** Ensure that Development is included in the UX process by offering the opportunity to participate in and understand the outcomes of user research, give early feedback on upcoming designs, and participate in design system strategy.
* **Goal setting:** Define value-driven quarterly UX OKRs, and manage their execution.
* **Public presence:** Represent the company at conferences, in media, in blog articles, in YouTube videos, and in other public venues.
* **Skip levels:** Hold regular skip-level 1:1s with all members of their team.
* **Team building:** Hire and retain a world-class team of Product Designers, UX Researchers, Technical Writers, and their managers.

### Requirements

* A minimum of 10 years experience managing designers, researchers, and writers and leading design for a product company.
* Solid visual awareness with understanding of basic design principles like typography, layout, composition, and color theory.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter.
* Interview with a manager on the UX Leadership team. In this interview, the interviewer will spend a lot of time trying to understand the experience you have leading managers, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your work, experience with Design Systems, and technical ability, too.
* Interview with a director on the UX Leadership team. In this interview, we will be looking for you to give some real insight into a problem you were solving as part of a large initiative you led the work on. We'll look to understand the size and structure of the team, the goals of the project, how you/the team approached research, how you used research to inform decisions, and how you collaborated with the wider team. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with VP of Product.
* Interview with VP of Engineering.

As always, interviews and the screening call will be conducted via video.

See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
