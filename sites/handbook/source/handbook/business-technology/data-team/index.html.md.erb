---
layout: handbook-page-toc
title: "Data Team"
description: "The GitLab Data Team is responsible for building and operating data and analytics infrastructure to help power decision making across GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

##  Welcome to the Data Team Handbook

* Our Mission is to **Deliver Results That Matter With Trusted and Scalable Data Solutions.**
* Read our [Direction](/handbook/business-technology/data-team/direction/) page to learn _what_ we are doing to improve data at GitLab.
* Our [Principles](/handbook/business-technology/data-team/principles/) inform how we accomplish our mission.

**Would you like to contribute? [Become a Data Champion](/handbook/business-technology/data-team/direction/data-champion/), [recommend an improvement](https://gitlab.com/gitlab-data/analytics/-/issues), [visit Slack #data](https://gitlab.slack.com/messages/data/), [watch a Data Team video](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI). We want to hear from you!**
{: .alert .alert-success}

## Navigating The Data Team Handbook

The Data Team Handbook contains a large amount of information! To help you navigate the handbook we've organized it into the following major sections:

<div class="flex-row" markdown="0" style="height:110px;">
  <a href="/handbook/business-technology/data-team/data-catalog/" class="btn btn-purple" style="width:200px;margin:5px;display:flex;align-items:center;height:100%;">Dashboards & Data You Can Use</a>
  <a href="/handbook/business-technology/data-team/#how-data-works-at-gitlab" class="btn btn-purple" style="width:200px;margin:5px;display:flex;align-items:center;height:100%;">How Data Works At GitLab</a>
  <a href="/handbook/business-technology/data-team/how-we-work" class="btn btn-purple" style="width:200px;margin:5px;display:flex;align-items:center;height:100%;">How The Data Team Works</a>
  <a href="/handbook/business-technology/data-team/platform/" class="btn btn-purple" style="width:200px;margin:5px;display:flex;align-items:center;height:100%;">How The Data Platform Works</a>
  <a href="/handbook/business-technology/data-team/direction/" class="btn btn-purple" style="width:200px;margin:5px;display:flex;align-items:center;height:100%;">What The Data Team Is Working On</a>
</div>

## How Data Works at GitLab

The collective set of people, projects, and initiatives focused on advancing the state of data at GitLab is called the **GitLab Data Program**. GitLab has two primary distinct teams within the Data Program who use data to drive insights and business decisions. These teams are complementary to one another and are focused on specific areas to drive a deeper understanding of trends in the business. The two teams are the (central) Data Team and, separately, analysts located in Sales, Marketing, Product, Engineering or Finance.

- The **Data Team** reports into Business Technology and is the Center of Excellence for analytics, analytics technology, operations, and infrastructure. The Data Team is also responsible for analytics strategy, building [enterprise-wide data models](/handbook/business-technology/data-team/platform/edw/), providing [Self-Service Data](/handbook/business-technology/data-team/direction/self-service/) capabilities, maintaining the [data platform](/handbook/business-technology/data-team/platform/#our-data-stack), developing [Data Pumps](/handbook/business-technology/data-team/platform/#data-pump), and monitoring and measuring [Data Quality](/handbook/business-technology/data-team/data-quality/). The Data Team is responsible for data that is defined and accessed on a regular basis by GitLab team members from the [Snowflake Enterprise Data Warehouse](/handbook/business-technology/data-team/platform/#data-warehouse). The Data Team builds data infrastructure to power approximately 80% of the data that is accessed on a regular basis.

- **Sales, Marketing, Product, Engineering and Financial Analysts**, or “Function Analysts” reside in their respective functions. These analysts perform specific analysis for data that is within the function. These analysts will build dashboards based on the centralized trusted data model for regular analysis, and will also build function-specific/ad-hoc data models to solve pressing needs. Function analysts work closely with the Data Team. Function analyst team members provide requirements for new trusted data dashboards, validate metrics, and help drive prioritization of work asked of the Data Team. When data gaps are found in our business processes and source systems, the team members will provide requirements to product management, sales ops and marketing ops to ensure the source systems capture the correct data.

### How Data Teams Work Together

On a normal operational basis, the Data Team and Function Analyst teams work in a "Hub & Spoke" model, with the Data Team serving as the "Hub" and Center of Excellence for analytics, analytics technology, operations, and infrastructure, while the "Spokes" represent each Division or Departments Function analysts. Function analysts develop deep subject matter expertise in their specific area and leverage the Data Team when needed.  From time to time, the Data Team provides limited development support for GitLab Departments that do not yet have dedicated Function Analysts or those teams which do have dedicated Function Analysts, but might need additional support. The teams collaborate through [Slack Data Channels](/handbook/business-technology/data-team/#data-slack-channels), the [GitLab Data Project](https://gitlab.com/gitlab-data/), and ad-hoc meetings.

```mermaid
classDiagram
    Sales <|-- Data
        Sales : +Sales Analyst
    Marketing <|-- Data
        Marketing : +Marketing Analyst
    Product <|-- Data
        Product : +Product Analyst
    Engineering <|-- Data
        Engineering : +Operations Analyst
        Engineering : +Infrastructure Analyst
    Finance <|-- Data
        Finance : +Financial Analyst
    People <|-- Data
        People : +People Analyst
    Data : +Data Analyst
    Data : +Analytics Engineer
    Data : +Data Engineer
    Data : +Data Scientist
```

### Data Fusion Teams

To support stable business workflows and longer-term initiatives, the Data Team has deployed three sub-teams, or **Data Fusion Teams**. A single Data Fusion Team includes members from Division leadership, Function Analysts, and the Data Team. Data Fusion Teams collaborate towards development of data solutions, including Dashboards, Data Models, Data Pipelines, Data Pumps, and ad-hoc analysis. Data Fusion Teams are staffed with the appropriate set of team members required to develop a full data solution. The Data Fusion Teams meet on a weekly basis, define, review, and align on priorities, define quarterly objectives, and collaborately develop successful data solutions.

- The **Go-To-Market** Data Fusion Team focuses on the activities required to bring GitLab's products and services to the marketplace and measuring the business performance of these activities. The team meets on a weekly basis and covers topics defined in the [GTM Data Weekly Agenda](https://docs.google.com/document/d/1m-SygSyUqEcfd276_Jb1ZARMXIb4Z8clDF0eoxmudyw/edit#), while the teams priorities are defined in the [GTM Data Priorities](https://docs.google.com/spreadsheets/d/1EyjWw3-0iFt0oil95U15X8rFGj1aKrSYVYV1GMzWfq4/edit#gid=1029287785) sheet. Data Team staff allocation is defined in [Team Organization](/handbook/business-technology/data-team/organization/#-team-organization).

- The **Research & Development** Data Fusion Team focuses on understandung how GitLab's products are used by customers, towards the goal of developing complete customer intelligence and product intelligence capabilities to make better decisions. The team meeets on a weekly basis and covers topics defined in the [R&D Data Weekly Agenda](https://docs.google.com/document/d/1CRIGdNATvRAuBsYnhpEfOJ6C64B7j8hPAI0g5C8EdlU/edit#), while the teams priorities are defined in the [R&D Data Priorities](https://docs.google.com/spreadsheets/d/1hCAY4tW0vbsYadHQzoGQ59aZmcMz2daumvuH3JwgarA/edit#gid=2036283287) sheet. Data Team staff allocation is defined in [Team Organization](/handbook/business-technology/data-team/organization/#-team-organization).

- The **General & Administrative** Data Fusion Team focuses on organization performance of GitLab teams, including People metrics. The G&A Fusion Team is currently `under construction` and is expected to launch in late FY22. The team currently meets on a weekly basis as part of the [Engineering & Data Team Weekly Sync](https://docs.google.com/document/d/1ekImsdsldcYNnNhWkI0pTZldjgRoJDaq6odo2BgKhQM/edit?usp=sharing). Data Team staff allocation is defined in [Team Organization](/handbook/business-technology/data-team/organization/#-team-organization).

### The Data Platform Team

The **Data Platform Team** is a critical team within the larger Data Team and focuses on development and operations of [data infrastructure](/handbook/business-technology/data-team/platform/). The Data Platform Team is both a development team and an operations/site reliability team. The team supports all Data Fusion Teams with **available, reliable, and scalable** data compute, processing, and storage. Platform components include the Data Warehouse, New Data Sources, Data Pumps, Data Security, and related new data technology. The Data Platform Team is composed of [Data Engineers](https://about.gitlab.com/job-families/finance/data-engineer/).

### Data Job Families

The job families are designed to support all of the routine activities expected of a Data Team. In FY22 we are introducing two new job families, Data Scientist and Analytics Engineer.

- [Data Analyst](https://about.gitlab.com/job-families/finance/data-analyst/)
- [Data Scientist](https://about.gitlab.com/job-families/finance/data-science)
- Analytics Engineer (`under construction`) 
- [Data Engineer](https://about.gitlab.com/job-families/finance/data-engineer/)
- [Manager, Data](https://about.gitlab.com/job-families/finance/manager-data/)
- [Director, Data](https://about.gitlab.com/job-families/finance/dir-data-and-analytics/)

## How To Connect With Us

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://gitlab.slack.com/messages/data/" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Primary #Data Slack Channel</a>
  <a href="https://gitlab.com/gitlab-data/analytics/-/issues" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Issue tracker</a>
  <a href="https://www.youtube.com/playlist?list=PL05JrBw4t0KrRVTZY33WEHv8SjlA_-keI" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">GitLab Unfiltered Data Team Playlist</a>
  <a href="https://www.worldtimebuddy.com/?pl=1&lid=2950159,5746545,4180439,1271439" class="btn btn-purple" style="width:30%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">What time is it for folks on the data team?</a>
</div>
<br>

### Data Slack Channels

- [#data](https://gitlab.slack.com/messages/data/) is the primary channel for all of GitLab's data and analysis conversations. This is where folks from other teams can link to their issues, ask for help, direction, and get general feedback from members of the Data Team.
- [#data-daily](https://gitlab.slack.com/messages/data-daily/) is where the Data Team tracks day-to-day productivity, blockers, and fun. Powered by [Geekbot](https://geekbot.com/), it's our asynchronous version of a daily stand-up, and helps keep everyone on the Data Team aligned and informed.
- [#data-lounge](https://gitlab.slack.com/messages/data-lounge/) is for links to interesting articles, podcasts, blog posts, etc. A good space for casual data conversations that don't necessarily relate to GitLab. Also used for intrateam discussion for the Data Team.
- [#data-engineering](https://gitlab.slack.com/messages/data-engineering/) is where the GitLab Data Engineering team collaborates.
- [#business-technology](https://gitlab.slack.com/messages/business-technology/) is where the Data Team coordinates with Business Technology in order to support scaling, and where all Business Technology-related conversations occur.
- [#analytics-pipelines](https://gitlab.slack.com/messages/analytics-pipelines/) is where slack logs for the ELT pipelines are output and is for data engineers to maintain.  The DRI for tracking and triaging issues from this channel is shown [here](/handbook/business-technology/data-team/platform/infrastructure/#data-infrastructure-monitoring-schedule)
- [#dbt-runs](https://gitlab.slack.com/messages/dbt-runs/), like #analytics-pipelines, is where slack logs for [dbt](https://www.getdbt.com/) runs are output. The Data Team tracks these logs and triages accordingly.
- [#data-triage](https://gitlab.slack.com/messages/data-triage/) is an activity feed of opened and closed issues and MR in the data team project.
- [#data-decisive-data-projects](https://gitlab.slack.com/messages/data-decisive-data-projects/) is where we discuss major data development projects, including our trusted data enterprise dimensional model.

You can also tag subsets of the Data Team using:

- @datateam - this notifies the entire Data Team
- @data-engineers - this notifies just the Data Engineers
- @data-analysts - this notifies just the Data Analysts

Except for rare cases, conversations with folks from other teams should take place in #data, and possibly the fusion team channels when appropriate.  Posts to other channels that go against this guidance should be responded to with a redirection to the #data channel, and a link to this handbook section to make it clear what the different channels are for.

### GitLab Groups and Projects

The Data Team primarily uses these groups and projects on GitLab:

- [GitLab Data](https://gitlab.com/gitlab-data) is the main group for the GitLab Data Team.
- [GitLab Data Team](https://gitlab.com/gitlab-data/analytics) is the primary project for the GitLab Data Team.

You can tag the Data Team in GitLab using:

- @gitlab-data  - this notifies the entire Data Team
- @gitlab-data/engineers  - this notifies just the Data Engineers
- @gitlab-data/analysts - this notifies just the Data Analysts

### Team, Operations, and Technical Guides

|  **TECH GUIDES** | **INFRASTRUCTURE** | **DATA TEAM** |
| :--------------- | :----------------- | :-------------- |
| [SQL Style Guide](/handbook/business-technology/data-team/platform/sql-style-guide/) | [High Level Diagram](/handbook/business-technology/data-team/platform/#our-data-stack) | [How We Work](/handbook/business-technology/data-team/how-we-work/) 
| [dbt Guide](/handbook/business-technology/data-team/platform/dbt-guide/) | [System Data Flows](/handbook/business-technology/data-team/platform/infrastructure/#system-diagram) | [Team Organization](/handbook/business-technology/data-team/organization) 
| [Python Guide](/handbook/business-technology/data-team/platform/python-guide/) | [Data Sources](/handbook/business-technology/data-team/platform/#extract-and-load)| [Calendar](/handbook/business-technology/data-team/how-we-work/calendar/) 
| [Airflow & Kubernetes](/handbook/business-technology/data-team/platform/infrastructure/#common-airflow-and-kubernetes-tasks) | [Snowplow](/handbook/business-technology/data-team/platform/snowplow/)  | [Triage](/handbook/business-technology/data-team/how-we-work/triage/) 
| [Docker](/handbook/business-technology/data-team/platform/infrastructure/#docker) | [Permifrost](/handbook/business-technology/data-team/platform/permifrost) | [Merge Requests](/handbook/business-technology/data-team/how-we-work/duties/#merge-request-roles-and-responsibilities) 
| [Data CI Jobs](/handbook/business-technology/data-team/platform/ci-jobs) | [DataSiren](/handbook/business-technology/data-team/platform/#datasiren) | [Planning Drumbeat](/handbook/business-technology/data-team/planning/) 
| [SiSense Style Guide](/handbook/business-technology/data-team/platform/sisense-style-guide/) | [Trusted Data](/handbook/business-technology/data-team/platform/#tdf)
| [Learning Library](/handbook/business-technology/data-team/learning-library/)

## Data Team Handbook Structure

- [Dashboards & Data You Can Use](/handbook/business-technology/data-team/data-catalog)
- [Data Learning and Resources](/handbook/business-technology/data-team/learning-library)
- [How The Data Team Works](/handbook/business-technology/data-team/how-we-work)
    - [Planning Drumbeat](/handbook/business-technology/data-team/planning)
    - [Calendar](/handbook/business-technology/data-team/how-we-work/calendar)
    - [Team Duties](/handbook/business-technology/data-team/how-we-work/duties)
    - [Data Analytics Team](/handbook/business-technology/data-team/organization/analytics/)
    - [Data Engineering Team](/handbook/business-technology/data-team/organization/engineering)
    - [Data Team Principles](/handbook/business-technology/data-team/principles)
    - [Data Handbook Documentation](/handbook/business-technology/data-team/documentation)
- [How The Data Platform Works](/handbook/business-technology/data-team/platform)
    - [Sisense (Periscope)](/handbook/business-technology/data-team/platform/periscope)
    - [dbt Guide](/handbook/business-technology/data-team/platform/dbt-guide)
    - [Enterprise Data Warehouse](/handbook/business-technology/data-team/platform/edw)
    - [Data Infrastructure](/handbook/business-technology/data-team/platform/infrastructure)
    - [SQL Style Guide](/handbook/business-technology/data-team/platform/sql-style-guide)
    - [Python Guide](/handbook/business-technology/data-team/platform/python-guide)
    - [Permifrost](/handbook/business-technology/data-team/platform/permifrost)
    - [Snowplow](/handbook/business-technology/data-team/platform/snowplow)
    - [Data CI Jobs](/handbook/business-technology/data-team/platform/ci-jobs)
    - [Sisense Style Guide](/handbook/business-technology/data-team/platform/sisense-style-guide)
    - [Trusted Data Framework](/handbook/business-technology/data-team/platform/dbt-guide/#trusted-data-framework)
- Data Programs and Services
    - [Data for Finance](/handbook/business-technology/data-team/programs/data-for-finance)
    - [Data for Product Managers](/handbook/business-technology/data-team/programs/data-for-product-managers)
    - [Data Champion](/handbook/business-technology/data-team/direction/data-champion/)
    - [Data Quality](/handbook/business-technology/data-team/data-quality)
    - [Data Services](/handbook/business-technology/data-team/data-service)
