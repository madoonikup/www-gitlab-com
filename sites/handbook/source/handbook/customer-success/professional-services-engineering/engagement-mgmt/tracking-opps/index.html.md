---
layout: handbook-page-toc
title: "Professional Services EM Opportunity Tracking"
description: "Describes processes for tracking open PS opportunities."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Tracking Opportunities

:warning: This page is under construction

## Salesforce.com Tracking

Tracking the overall pipeline for Professional Services and individual opportunities is important to the Professional Services team to retain visbility and ensure capacity for future projects.
This importance increases as we approach the end of the financial quarter (FQ), as we typically see a spike in **Closed Won** deals at this time.

To help this visibilty, the Engagement Managers maintain a subset of fields against an opportunity under the `Professional Services` section. These fields are reviewed and updated on a regular basis to support reporting to Professional Services leadership on the current forecast:

- `Engagement Manager` - Denotes the Lead Engagement Manager supporting the opportunity
- `EM Confidence`, this can be set as:
    - **Grey** (Default), meaning the EM does not have visibility or awareness of the opportunity
    - **Red**, meaning in the opinion of the EM, this will not close won within the timeframe stated
    - **Amber**, meaning the EM is confident the oportunity will close won, but not necessarily within the timeframe stated
    - **Green**, meaning the EM is condient that the opportunity will close won within the time started
- `EM Confidence Rationale` - Context and rationale to support the `EM Confidence` field
- `Scoping Issue Link` - A direct link to the supporting scoping issue

