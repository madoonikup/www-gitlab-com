---
layout: handbook-page-toc
title: "Education Program"
description: "The GitLab for Education Program provides the top tiers of GitLab for free to students and faculty at educational institutions around the globe. "
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## About

At GitLab, we believe that **every student can contribute**! The [GitLab for Education Program](/solutions/education/) provides the top tiers of GitLab for free to students and faculty at educational institutions around the globe. We are invested in ensuring that students have access to the full functionality of GitLab while in school so they can become future contributors and evangelists of GitLab.

The GitLab for Education Program has exceeded our expectations on its own merit. As of January 2020 we reached over **740 educational institutions worldwide** and have **1.4 million users**.

## How to reach us
* DRI: Christina Hupy - @c_hupy on GitLab
* Slack channel: #community-programs
* Email: education@gitlab.com

## Mission
The primary mission of the GitLab for Education Program is to facilitate and drive the adoption of GitLab at educational institutions around the globe and build an engaged community of GitLab evangelists and contributors in the next generation of the workforce.

Additionally, the Education Program seeks to evangelize the benefits of an all-remote operating model and GitLab's associated company values to the next generation of the workforce.

### Goals
The goals in building out the Education Program are:
* Align the program structure and license offerings with the needs and operating models of educational institutions while providing the best GitLab has to offer to students, faculty, and staff.
* Grow the base of educational institutions, students, faculty, and staff using GitLab.
* Build a robust and engaged educational community full of members who collaborate, contribute, and enable each other to be successful with GitLab.
* Build meaningful relationships with the Education Program member institutions.
* Provide a wealth of resources for adopting GitLab in an educational setting including course materials, case studies, code examples, syllabi, and presentations.
* Evangelize the benefits of DevOps as a discipline and GitLab as the leading single application for the DevOps lifecycle.
* Bring DevOps into the classroom in related disciplines such as computer science and infrastructure technology.
* Be a thought leader in the discipline of DevOps by engaging with related academic disciplines, academic organizations, and associations.
* Evangelize the benefits of an all-remote operating model and GitLab's associated company values to the next generation of the workforce.

## Vision
The vision of the GitLab for Education Program is to enable educational institutions to be successful in teaching, learning, conducting research with GitLab. We seek to build an engaged community of GitLab users around the world who actively contribute to GitLab and each other’s success, and ultimately become evangelists of GitLab in the workplace and beyond.

## What we are working on
GitLab for Education Program issues typically exist in the [Education Program subgroup](https://gitlab.com/gitlab-com/marketing/community-relations/education-program) of the [Community Relations Group](https://gitlab.com/gitlab-com/marketing/community-relations) but they can also exist in [Field Marketing](https://gitlab.com/gitlab-com/marketing/field-marketing), [Corporate Marketing](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing), or other [marketing subgroups](https://gitlab.com/gitlab-com/marketing).

We use the `Education Program` label to track issues. The Education Program [issue board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/education-program/-/boards) provides an overview of these issues and their status. Any [Epics](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics) that we are working on can be found in the Community Relations Group with the tag `Education Program`.

We use [Epic Boards](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?label_name%5B%5D=OKR) in the Community Relations project to track our Education Program OKRs. All OKR Epics specific the Education Program have the 'education' tag.

### Aditional Labels Used by the Education Team

| Label  | Use  |   
|---     |---   |
|`EDU Twitch`   | Related to the Twitch Channel  |
|`non profit`  | Related to non-profits, as opposed to universities or schools  |    
|`Edu OKR`    | Realted to OKRs for the quarter   |

## Key Performance Indicators

### Number of educational institutions enrolled per quarter
This KPI is defined as the total number of educational institutions that are issued an educational license per quarter.

At this time we are not tracking renewals but we are looking at improving data accuracy and will include renewals on a subsequent iteration.

### Number of educational seats enrolled per quarter
This KPI is defined as the total number of seats issued for all educational licenses per quarter.

## GitLab for Education Program Requirements
In order to qualify for the GitLab for Education Program applicants must meet the GitLab for Education Program Requirements. Once accepted in the program, institutions must agree and are subject to the [GitLab for Education Program Agreement](/handbook/legal/education-agreement/). 

* **Be a qualified educational institution**: A qualified educational institution is one that has been accredited by an authorized agency within its applicable local, state, provincial, federal, or national government and has the primary purpose of teaching its enrolled students. Qualified educational institutions can be public or private and must be non-profit/non-commercial.

* **Meet the use case requirements**: The GitLab educational license can be used solely for the purposes of **instructional use** or **non-commercial academic research**. Instructional use includes activities related to learning, training, research and development.  Non-commercial academic means conducting not-for-profit research projects conducted by the program member, and not at the request of a third party, which are not intended to, or in fact, produce results, works, services, or data for commercial use by anyone to generate revenue, or for the benefit of a third party.

The GitLab educational license cannot be used for commercial, professional, or any other for-profit purposes. **Specifically, it is not authorized for use to run, administer, or operate an institution.**

* **Be a full-time faculty or staff member at the qualified educational institution**: We can only issue licenses to full-time faculty or staff who are directly employed full-time at the educational institution. Students who may also be employed in a staff role cannot apply.

**Please note that the decision to issue a GitLab education license is always at the discretion of GitLab. If you have questions on the application and decision making process, please reach out to education@gitlab.com.**

**Examples of educational institutions that qualify:**
* K-12 institutions include Elementary, Middle, and High Schools which may include both publicly and privately funded - **Note**: GitLab.com (SaaS software) is not available to K-12 institutions. However, K-12 institutions may apply for GitLab self-managed instances.
* Junior Colleges, Community Colleges, and Technical Schools
* Universities
* Research Institutes or Centers directly *owned and operated by* a University

**Examples of entities that do not qualify:**
* Training centers
* Churches and libraries
* Medical research centers or institutions associated with a hospital or health care system
* Independent research laboratories
* Re-Training programs
* Military schools, institutes, or training centers within a branch of the military
* Research institutes or National Laboratories that are owned by a Federal Government
* eLearning platforms not directly affiliated with or operated by an accredited academic University
* Coding academies, bootcamps, or independent learning platforms

**Examples of acceptable use cases:**
* Classroom use - all activities related to the instruction of students in the classroom
* Non-commercial academic research - activity related to not-for-profit research projects at an educational institution that are not conducted for the benefit of a third part. 
* Organizational use - activity related to a club or organization at an educational institution as related to the development of students; this could include open source student clubs, robotic clubs, engineering clubs or the like. Note that the non-acceptable use cases still apply to clubs and organizations.  

**Examples of non-acceptable use cases:**
* Information technology /  professional use - use for maintaining or running the infrastructure, technology or otherwise, of the institution
* Administrative use - use for any administrative functions of the institution including program management, planning, marketing, and service delivery
* Commercial research - research conducted by any commercial programs or entities operated by or affiliated with the educational institution for a commercial purpose . 
* Third party directed research - For example, the US Government may open a request for proposals for a specific project with a well-defined results. Research laboratories, owned and operated by a University such as an Advanced Research Laboratory (ARL), may apply and be awarded the project in the form of a contract. This type of work, with the goal of benefitting a third party through producing a given outcome, rather contributing to the general body of scientific research itself, does not qualify. An Advanced Research Laboratory or entity of the like, can still hold and apply for a GitLab for Education license but any work completed for the benefit of a third party, or under contract for example, cannot occur on the free license.
* Services  - Any activities conducted by a consulting center, super computer laboratory, or entity that provides services for the benefit of a third party are not acceptable under the free license.


### Students
At this time, GitLab does not issue licenses directly to students as part of the GitLab for Education Program. Students are welcome to encourage their educational institution to apply to the program directly. Students can access a [free subscription for GitLab.com](/pricing/#gitlab-com) or a [free download of our core self-managed offering](/pricing/#self-managed). Students can also apply for a [30-day trial](https://gitlab.com/-/trials/new) if they would like to try out some more advanced features.

Children under the age of 13 are not permitted to use the GitLab.com (SaaS Software).

### Non-profits
At this time, GitLab does not have a formal not-for-profit program. The primary reason is that the volume of applications to our current free programs takes up all of our bandwidth! We do generally see the value in non-profits and we hope to create some sort of a non-profit offering in the future.

1. To stay up-to-date, you can subscribe to the [the non-profit issue](https://gitlab.com/gitlab-com/marketing/community-relations/education-program/general/-/issues/17) by enabling notifications. To enable notifications, click the Notifications toggle in the sidebar to on.
2. You can contribute your feedback.  

Any inbound request for a discounted or free license by a not-for-profit organization is handled on a case-by-case basis by the DRI (the appropriate sales team member for the geographic region of the non-profit and their manager). We cannot guarantee that non-profit requests will be granted as the decision is at the discretion of the DRI. Discounted or free licenses for non-profits are only issued in very rare cases. Because GitLab does not currently have a license and end-user license agreement (EULA) specifically for non-profits, we are not able to issue a license without additional support from our legal team in order to modify our existing EULA and license granting process.

## Resources for GitLab for Education Program Participants
We are always looking for ways to better support our participants' use of GitLab for teaching, learning, and research. Please reach out to us at education@GitLab.com with any ideas.

### Student Spotlight Program
The Student Spotlight Program provides an opportunity for students, faculty, and staff to feature their work on GitLab! The aim of the program is two-fold. First, we hope to inspire other to join the program by showcasing some of the exciting work our members are doing. Secondly, we want to connect and build relationships with institutions already using GitLab for teaching and learning.

Students, faculty, or staff can apply to be highlighted in the Students Spotlights Program by filling out the [application form](https://docs.google.com/forms/d/e/1FAIpQLSdHzG0IEDw6VlUQDqWsweNRDIdM2HQpoBH-t2OzK1m0_SMeiQ/viewform). Our team will review the applications and selected projects will be featured via a video-recorded interview with the students and professors involved. Interviews and project links will be featured on the GitLab Unfiltered YouTube channel and the GitLab for Education main webpage.

### How to structure your projects
We often receive questions about how to best manage your licenses. Here are a few tips:
* GitLab does not use a named license model. This means that seats are generic and not specific to a user. If a user leaves your organization, you can remove or block that user to free the seat. This seat can then be used by another user.
* Although we offer only one license key for the self-managed solutions, this key can be used on multiple independent instances. This means you can run multiple separate servers with the same license key. The only caveat is that currently there isn't an easy way to calculate the total number of consumed seats across all instances.
- You can manage the visibility of your projects with GitLab groups. A member of the parent group automatically has access to all descendants. GitLab doesn't support having the subgroup be more restrictive than its parent. However, being a part of a subgroup does not grant you access to the parent group. The best way to organize your work is to make everyone a member of their respective subgroup having only admins in the organizational (top-level) group. [Learn more about GitLab groups].(https://docs.gitlab.com/ee/user/group/).
- Please also see our [licensing and subscription FAQ](/pricing/licensing-faq/) section for more details.

## Internal Program Processes

### Workflows for Processing Applications and Renewals

All applications to our Education Program and renewal requests for education licenses are routed through our application [workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/).

### OKR and KR Organization

OKR Epics are located in the [Community Relations](https://gitlab.com/gitlab-com/marketing/community-relations) subgroup. We use the `Education Program` and the `OKR` label on each OKR epic. Each KR will be added as an issue to the [`EDU Key Results`](https://gitlab.com/gitlab-com/marketing/community-relations/education-program/edu-key-results/-/issues) project within the [Education Program](https://gitlab.com/gitlab-com/marketing/community-relations/education-program) sub-group. Each KR issue should have a milestone and an assignee at the time of creation. If there is a question about who the DRI is for a KR, assign the Program Manager until a DRI is decided upon. 

The roles, KR assignees, and other details should be covered in the first Education Team meeting (or 1-on-1) of each quarter. There, create a todo list for each member of the team to cover Milestones, KR Issues, and OKR Epics. This organization ensures a clear direction and performance for the quarter. 

Every Quarter there should be a new epic called "Outreach Formats and Content FYXX QX" where we keep track of the content created for the quarter. This includes articles, videos, talks, and any shareable content. Content creators have the option of adding a comment for each piece of content or creating an issue, however, [consider an issue first.](https://about.gitlab.com/handbook/marketing/project-management-guidelines/issues/)

A table should be created in the description of the epic. The columns are `Type of Outreach` and `Views.` Views will represent whatever measureable metric being used for that patricular content. When publishing new content or planning for it, please add it to the table. Once metrics are available, please update the appropriate column; this should be done within a month after publishing or releasing the content. All Twitch content will be stored in it's own epic separate from Quarterly organization. 

## SubGroup Organization

The `Education Team` subgroup should contain the following projects for organizational purposes. 

| Project | Purpose |
|---------|---------|
| KeyResults| Store issues related to Key Results|
| ProgramMembersCommunications | Issues related to communicating with program members including studetns, professors, fraternitis and educational organizations|
| Non-Profit |  All non profit orgniazation outreach and work| 
| Outreach   | includes blogs, videos, podcasts, and other methods of connecting with the community|
| ShinyRocks | Non-OKR related ideas and "nice-to-haves" |

### Metrics
The Education Program generates standard metrics for our KPIs and for general monitoring purposes. These metrics are reported in the Community Relations Group Conversation as well as for quarterly tracking.

The steps below will generate the Education Program metrics:  

1. Log into SFDC.
1. Navigate to Reports.
1. Open the 'Education Opportunities Metrics' [SFDC Report](https://gitlab.my.salesforce.com/00O4M000004e4KE).
1. Click `Run Report` to generate the report.
1. Click `Export Details`. Choose `Unicode (UTF-8)` for the Export File Encoding and `.csv` for the Export File Format. Then click `Export` and save the file.
1. Download the [Eduoss](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/blob/master/tools/edoss/edoss.rb) script.
1. Open a command prompt and change to the directory where you have the script and .csv file.
1. Run the Edoss script with the exported csv file as input to generate the final csv file.
       `./edoss.rb -i <exported_file>.csv -o <output_file>.csv`
1. Update the [GitLab for Education Graphs](https://docs.google.com/spreadsheets/d/18zudgYDeL1Zy90mEIO_Xi8X-ZyE86N-eQ1Nluyrh1hY/edit#gid=1612197101) for the Community Relations Group Call by importing the results of the script and updating the figures.
Notes:
     - You may need to make the file executable by using the following command:
       `chmod 755 edoss.rb`
- Additionally, the processor script can provide the data in `yaml` and `json` formats.

### GitLab in Academic Research Citation Index
We have an MVC [GitLab in Academic Research Citation Index](https://about.gitlab.com/solutions/education/##gitlab-in-research) on the GitLab for Education Home page.  EndNote web will serve as the SSoT for the GitLab in Academic Research Citation Index displayed on our webpage. EndNote is an online tool that stores citations, allows collaboration, and has search functions. An added benefit of EndNote web is that we can export the citations in any standard citation format.

The workflow for updating and tracking the citation index is below.

You'll need to create an EndNote account from the [EndNote homepage](https://endnote.com/) and notify Christina Hupy who will share the **GitLab Citation Index Group** with you.

#### Finding and Adding Citations to the Citation Index

1. Find relevant citations on [Google Scholar](https://scholar.google.com/).
1. Save the citations your Google Scholar Library
1. Export the citations in EndNote format.
1. Log into your [EndNote account](https://endnote.com/). See note below if you do not have an account. Import the citations from Google Scholar
  - From the `Collect` menu go to `Import References`
  - Choose the File
  - For `Import Option` choose EndNote
  - For `To` choose **GitLab Citation Index Webpage** Group
1. View each citation from the `My References` menu
1. Update any relevant information that may have not imported correctly
1. Add relevant keywords
1. Save the citation

#### Adding Citations to the WebPage
1. Prepare a row in the citation index table on the webpage to store the new citation
1. In EndNote Web, go to `Format` and `Bibliography`
1. In `References` choose GitLab Citation Index Webpage
1. In `Bibliographic style` choose `Cite Them-Right Harvard`
1. In `Format Style` choose `HTML`
1. Preview and Print the index
1. Find the relevant citation
1. Copy and paste the citation into the placeholder
1. Update the `Notes` field in the citation record in EndNote with `added to website` so we are able to track which citations have been added.
