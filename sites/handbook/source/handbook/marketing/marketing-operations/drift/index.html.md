---
layout: handbook-page-toc
title: "Drift"
description: "Drift is a chat platform used by the Sales Development (SDR) organization to engage visitors on select webpages."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Drift is a chat platform used by the Sales Development (SDR) organization to engage visitors on select webpages. Engagement with Drift begins when a site visitor opts into interacting with an available Drift playbook. Playbooks are customized workflows that help visitors find information as well as offer the option to chat directly or schedule time to speak with SDRs. 

### Playbooks
We have Drift playbooks in use on select permanant webpages as well as temporary landing pages. The content of the playbook depends on the intended experience on the page and the target audience.

| Permanent pages | Playbook(s) |
| ------ | ------ |
| https://about.gitlab.com/pricing/ | ABM, AMER, APAC, EMEA, & LATAM |
| https://about.gitlab.com/sales/ | ABM, AMER, APAC, EMEA, & LATAM |
| https://about.gitlab.com/features/ | ABM, AMER, APAC, EMEA, & LATAM |
| https://about.gitlab.com/stages-devops-lifecycle/ | ABM, AMER, APAC, EMEA, & LATAM |
| https://about.gitlab.com/free-trial/ | ABM, AMER, APAC, EMEA, & LATAM |


| Temporary pages | Playbook(s) |
| ------ | ------ |
| TBD | TBD |

#### Performance measurement
All Drift playbooks are associated with Saleforce campaigns to enable playbook performance to be measured outside of the Drift platform.

### Conversation routing
Drift is expected to mirror the lead routing logic in [LeanData](/handbook/marketing/marketing-operations/leandata/) as closely as possible. Our goal is to provide the best possible experience for site visitors by connecting them with their aligned resources in the SDR organization. This means site visitors during offline hours will be asked to schedule a meeting with their SDR.

The account lists and playbook routing rules that govern how most Drift conversations are routed need to be refreshed to reflect changes to SDR alignment. Because this is not an automated process, Marketing Operations needs to be informed of needed changes via a [SDR alignment change request](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=sdr_alignment_change_request) from SDR leadership or as needed based on changes made to our marketing strategy that impact account alignment.

## SDR resources for using Drift

### Rules of engagement  
-   Set yourself to away when done working / no longer want to receive chats. Please note: If you set yourself to away, prospects will still be able to interact with the bot, but once they answer the initial questions, they are automatically given the option to book time on your calendar. If you don’t set yourself to away they will have to wait while the bot attempts to notify you. 
-   SLA: please respond to chats within 30 seconds
-   If you are routed a chat from outside your territory, assist and qualify by continuing the conversation. If a lead is then created in Salesforce, change it to the appropriate owner.
-   In the event you aren't able to assist a site visitor on your own and escalation is needed, please reach out to your manager and/or the #mktgops slack channel.

### Best practices
* Ensure you set yourself to away when you won't be able to receive Drift notifications. The prospect will still be able to chat with the bot but will be given the option to book a time with you right away as opposed to waiting for the bot to ping you. 
* As people have the ability to book meetings with you if you are offline or don't respond fast enough, every morning look at the meetings that were scheduled. You will receive an email invite. If you want to reach out prior to the meeting or potentially reschedule, please email the prospect in advance. 
* Ensure that you have browser notifications enabled to ensure you can respond quickly. If you don't respond within two minutes, the prospect will be prompted to schedule time with you. *Note: if you are using the [Drift mobile app](https://gethelp.drift.com/hc/en-us/articles/360019664613-How-to-Use-the-Drift-Mobile-App) - turn notifications on within the app as well. 
* When available in Drift, we recommend having DataFox, Salesforce, the SSoT territory alignment, and Google Calendar open in other tabs to quckily look into things. 

### Working an inbound chat lead
1. Start the conversation with a professional greeting asking how you can help.
2. Begin to research the prospect with the information they have provided.
3. Use DataFox to begin confirming company details
4. Use Salesforce to see if this person is already in our system and in contact with another GitLabber (check for duplicates)
     - If you confirm that this prospect is note in your territory or wouldn't align to you, you can pull up the master territory spreadsheet to see which SDR should they should be working with. You can then add that SDR to [join the conversation](https://gethelp.drift.com/hc/en-us/articles/360019448174-How-to-Add-Participants-to-Conversations). If the appropriate SDR is unavailable, qualify to the best of your ability and ask if you can have the appropriate SDR or AE/SAL (for existing customers) reach out directly via email or phone. Relay this information with the appropriate GitLabber and share the lead/contact with them. (The Drift conversation will show up in the activity history section).
5. Qualify the prospect while assisting with their needs. 
6. If a meeting gets set up,  use the CQL “lightning” label on the right side of the drift browser to mark promising conversations. 

### Drift resources & training materials

**Drift Meetings configuration**

*  Follow [Drift's instructions](https://help-116779.drift.help/article/setting-up-your-drift-meetings/) to connect your calendar to the chat platform, set your availabilty, and update your personal meeting settings. If you have questions, please reach out to your manager or the #mktgops slack channel for assistance.

**Set yourself to available or away**

*  Change your status by clicking your avatar in the bottom left-hand corner. Whether you are available or away, you can still have your notifications on or off.
*  **Note:** If you close out of Drift, your status will not change. The only way to change your status is by updating in the tool.

**Chat notifications**
* There are various ways in which you can be notified of a chat. [This article](https://gethelp.drift.com/hc/en-us/articles/360019501974-How-to-Set-Up-Up-Your-Notifications-Preferences) will walk you through how to set up or change notifications. 

**Close or open a conversations**
*  You can change your conversation status by clicking the drop-down in the top right-hand corner of your conversation.

**Marking a chat as a CQL**
*  A CQL is a Chat Qualified Lead. 
*  On the right hand side of your conversations view, you will be able to see information about the site visitor. Right below their name, you will see the four options of CQL in the form of a lightning bolt. Please mark a lead with the red line for `unqualified lead`, one lightning bolt for `Inquiry`, and three lightning bolts for `MQL`. Refer to this [term glossary](/handbook/sales/field-operations/gtm-resources/#glossary) for a refresher on `Inquiry` and `MQL` definitions. 

**Sharing your calendar in chat**
*  At the right-hand side of the conversation toolbar there is a small calendar icon. Click there and you can share your own calendar or anyone else’s calendar that is connected in Drift.

## Have a question or request? 
Would you like to learn more about Drift? Are you interested in adding a Drift playbook to a new or existing webpage? Have you noticed a potential Drift problem you would like investigated? 
* The best place to start is to open an issue. If you're an SDR manager requesting a change to conversation routing because of a team member update, please open use the lead routing change request [template](/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment). Otherwise, please open an issue in the Marketing Operations project stating the reason for the issue, providing relevant details about the request and using the following labels:
    * `MktgOps::0 - To Be Triaged`: Starting point for any label that involves Marketing Operations
    * `Drift`: Used if directly related to the Drift chat platform
    * `SDR`: Used if your request is from or involves the SDR organization
* If you have a quick question, you can use the #mktgops Slack channel.
